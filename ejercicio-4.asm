%include "io.inc"
extern printf
section .data
vector       dq 4.3,2.4,12.5,22.3,8.32,"$"
valorParcial dq 0
suma         dq 0
fmt          db "la suma del vector es: %f", 10, 0
section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
    ;write your code here
    mov ecx, 0
    
    ciclo:
    fld qword [vector+ecx]
    fstp qword [valorParcial]
    mov eax, [valorParcial]    
    mov edx, 36 ;valor en ASCII de "$"
    cmp eax, edx
    je finalizar 
    fld qword [vector+ecx]
    fld qword [suma]
    fadd st0, st1
    fstp qword [suma]
    add ecx, 8
    jmp ciclo
    
    finalizar:
    push dword[suma+4]
    push dword[suma]
    push fmt
    call printf
    add esp, 12
    xor eax, eax
    ret