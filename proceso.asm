;%include "io.inc"
extern printf
section .data
t       dq 4.0
r       dq 0
valora  dq 0
valorb  dq 0
valorc  dq 0
vcuad   dq 0
multi   dq 0
resta   dq 0
xuno    dq 0
xdos    dq 0
dosa    dq 0
fmt     db "%f",10,0
fmt2    db " y %f",10,0 
section .text
global CMAIN
CMAIN:
            mov ebp, esp; for correct debugging
            ;write your code here
            xor eax, eax
            push ebp
            mov ebp, esp  
            fld dword [ebp+8]
            fstp qword [valora]
            fld dword [ebp+12]
            fstp qword [valorb]
            fld dword [ebp+16]
            fstp qword [valorc]
            ;B al cuadrado 
            fld qword [valorb]   
            fmul st0, st0
            fstp qword [vcuad]
            ;4.A.C
            fld qword [valora]
            fld qword [valorc]
            fld qword [t]
            fmul st0,st1
            fmul st0,st2                        
            fstp qword [multi]
            ;B al cuadrado - 4.A.C
            fld qword [multi]
            fld qword [vcuad]
            fsub st0, st1
            FABS
            ;raiz cuadrada            
            fsqrt
            fstp qword [resta] 
            ;-b
            fld qword [resta]
            fld qword [valorb]
            fchs 
            ;x1
            fsub st0, st1
            fstp qword [xuno]
            ;x2
            fld qword [valorb]
            fchs
            fadd st0, st1
            fstp qword [xdos]
            ;2.A
            fld qword [valora]
            fadd st0, st0
            fstp qword [dosa]
            ;x1 dividido 2.A
            fld qword [dosa]
            fld qword [xuno]
            fdiv st0, st1
            fstp qword [xuno]
            ;x2 dividido 2.A
            fld qword [dosa]
            fld qword [xdos]
            fdiv st0, st1
            fstp qword [xdos]
            ;imprimir 
            push dword [xuno+4] 
            push dword [xuno]
            push fmt
            call printf
            add ESP, 12
            push dword [xdos+4] 
            push dword [xdos]
            push fmt2
            call printf
            add ESP, 12
            ;final
            mov esp, ebp
            pop ebp
            ret
